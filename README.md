# vaccine-availability-checker-python

## About

This script automates visiting the Kroger website with a browser,
checking appointment availabilities for zipcodes in your state,
and notifying you via PushBullet notifications of available appointments.

Note that it does NOT book you an appointment, it simply notifies you of current availabilities and their locations.

Note that startup is slower in headless (`--headless`) mode due to an initial security check by Kroger, but it still works.

## Requirements

- Python 3
  - Pip
  - Virtualenv
- Google Chrome (version >=90)

## Setup

### Dependencies

```bash
virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

## Usage

```bash
usage: pkg.py [-h] [--headless] [--zipcodes ZIPCODES [ZIPCODES ...]] [--state STATE] [--birthday BIRTHDAY] [--pushbulletApiKey PUSHBULLETAPIKEY]              [--pushbulletDevice PUSHBULLETDEVICE]
optional arguments:
  -h, --help            show this help message and exit
  --headless            whether you want to run the browser in headless mode
  --zipcodes ZIPCODES [ZIPCODES ...]
                        zipcodes to search for, you must provide one or more zipcodes for your state
  --state STATE         state to search vaccine for, you must provide one state
  --birthday BIRTHDAY   your date of birth in the form: "mm/dd/YYYY" (e.g. 03/19/1968)
  --pushbulletApiKey PUSHBULLETAPIKEY
                        your pushhbullet API key used for notifications
  --pushbulletDevice PUSHBULLETDEVICE
                        your pushhbullet device nickname for notifications (note: note pushed to all devices if no specific device provided)
```
