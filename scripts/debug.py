"""Wrapper module that runs locally."""
import sys
sys.path.insert(0, '.')
from py_pkg.pkg import AvailabilityChecker

acc_creator = AvailabilityChecker(enable_headless=False)
