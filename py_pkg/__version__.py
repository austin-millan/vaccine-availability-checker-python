"""
__version__.py
~~~~~~~~~~~~~~

Information about the current version of the vaccine-availability-checker-python package.
"""

__title__ = 'vaccine-availability-checker-python'
__description__ = 'vaccine-availability-checker-python'
__version__ = '0.1.0'
__author__ = 'Austin Millan'
__author_email__ = 'austin.millan@protonmail.com'
__license__ = 'MIT'
__url__ = 'https://gitlab.com/austin-millan/vaccine-availability-checker-python'
