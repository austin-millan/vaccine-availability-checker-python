"""
py_pkg.pkg.py
~~~~~~~~~~~~~~~~
"""
import time
import undetected_chromedriver as uc
# uc.install()
# time.sleep(5)
import logging
import random
time.sleep(1)
from datetime import datetime

from pushbullet import Pushbullet
import argparse
logging.basicConfig(level=logging.INFO)
logging.getLogger('undetected_chromedriver').level = logging.INFO
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from seleniumwire import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
random.seed(datetime.now())


class Appointment:

    def __init__(self, kroger_data=""):
        self.store = ""
        self.general_area = ""
        self.distance = ""
        self.street_address = ""
        self.appointment_date_string = ""
        self.city_state_zip = ""
        self.pharmacy_phone = ""
        self.vaccine_available = False
        if kroger_data:
            self._parse_kroger(data=kroger_data)

    # dynamically determine fields
    def _parse_kroger(self, data=""):
        if not data or len(data) == 0:
            return
        line_split = data.splitlines()
        if len(line_split) < 4:
            logging.info("potential problems parsing appointment card")
            return
        self.general_area = line_split[0]
        self.store = "Fred Meyers"
        self.distance = line_split[1]
        self.appointment_date_string = line_split[2]
        if "No Available Appointments" in self.appointment_date_string:
            self.vaccine_available = False
        else:
            self.vaccine_available = True
        self.street_address = line_split[3]
        self.city_state_zip = line_split[4]

    def __str__(self):
        output = ""
        if len(self.appointment_date_string) > 0:
            output += f'Vaccine Available? {"Yes" if self.vaccine_available else "No"}\n'
        if len(self.appointment_date_string) > 0:
            output += f"Store: {self.store}\n"
        if len(self.appointment_date_string) > 0:
            output += f"Appointment Availability Status: {self.appointment_date_string}\n"     
        if len(self.street_address) > 0:
            output += f"Location: {self.street_address}, {self.city_state_zip}"
        return output


class AvailabilityChecker:
    """Initialize account creation"""
    def __init__(self, enable_headless=False, state="CA", zipcodes=["90011"], birthday="4/20/1969", pb_api_key="", pb_device_name=""):
        if not isinstance(state, str):
            raise TypeError("Expected string for state")
        if not isinstance(zipcodes, list):
            raise TypeError("Expected list for zipcodes")
        self.messages = []
        self.state = state
        self.zipcodes = zipcodes
        self.birthday = birthday
        self.pb = None
        self.pb_device = None
        self.driver = None
        datetime.strptime(self.birthday, "%m/%d/%Y")  # formatted for Kroger
        if pb_api_key:
            self.pb = Pushbullet(pb_api_key)
            if pb_device_name:
                if "\"" in pb_device_name:
                    pb_device_name = pb_device_name.replace("\"", "")
                found_device = False
                for device in self.pb.devices:
                    if device.nickname == pb_device_name:
                        found_device = True
                        self.pb_device = device
                        break
                if not self.pb_device:
                    raise ValueError(f"Unable to find PushBullet device with name: {pb_device_name}, perhaps try one of: {self.pb.devices}")
        uc.install()
        sw_options = {
            'verify_ssl': False,  # Verify SSL certificates but beware of errors with self-signed certificates
            'disable_encozipcodesding': True,  # Ask the server not to compress the response
            'disable_capture': True,  # Pass all requests straight through.
            'proxy': {
                # 'https': 'https://177.54.145.61:1080',
                # 'http': 'https://177.54.145.61:1080'
                # 'http': 'socks5://user:pass@192.168.10.100:8888',
                # 'https': 'socks5://user:pass@192.168.10.100:8888',
                # 'no_proxy': 'localhost,127.0.0.1'
            },
            # 'request_storage_base_dir': 'requests-dir'  # Use /tmp to store captured data
        }
        desired_capabilities = DesiredCapabilities.CHROME.copy()
        desired_capabilities['acceptInsecureCerts'] = True
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--ignore-certificate-errors')
        chrome_options.add_argument('--allow-running-insecure-content')
        chrome_options.add_argument("--incognito")
        # chrome_options.add_argument("start-maximized")
        # chrome_options.add_argument("--disable-extensions")
        # chrome_options.add_argument("--disable-plugins-discovery");
        # chrome_options.add_argument('--profile-directory=Default')
        if enable_headless:
            chrome_options.add_argument('--headless')
        # desired_capabilities=desired_capabilities,
        # emulate_touch=True, #only in uc
        self.driver = webdriver.Chrome(
            options=chrome_options,
            seleniumwire_options=sw_options,
        )

    def push_notification(self, message):
        if not self.pb:
            # notifications not configured, skip
            return
        else:
            if any(message in s for s in self.messages):
                # don't re-send the same message
                return
            if self.pb_device:
                self.pb.push_note("COVID-19 Vaccine Appointment Notification", message, device=self.pb_device)
            else:
                self.pb.push_note("COVID-19 Vaccine Appointment Notification", message)
            self.messages.append(message)

    def check_kroger(self):
        action = ActionChains(self.driver)
        self.driver.get("https://www.kroger.com/")
        time.sleep(3)
        sec_check = self.driver.find_elements_by_id('sec-text-if')
        sec_check_done = False
        if len(sec_check) > 0:
            for i in range(0, 10):
                self.driver.get("https://www.kroger.com/")
                time.sleep(20)
                sec_check = self.driver.find_elements_by_id('sec-text-if')
                if len(sec_check) > 0:
                    continue
                else:
                    sec_check_done = True
                    break
        else:
            sec_check_done = True
        if not sec_check_done:
            raise RuntimeError("Timeout exceeded for security check on Kroger homepage")
        for elem in self.driver.find_elements_by_xpath("//a[@href]"):
            if not elem:
                continue
            if "vaccine" in elem.get_attribute("href"):
                elem.click()
                time.sleep(5)
                break
        for elem in self.driver.find_elements_by_xpath("//a[@href]"):
            if not elem:
                continue
            if "eligibility" in elem.get_attribute("href"):
                elem.click()
                break
        # Terms of Use and Policy (I Agree / I Disagree)
        time.sleep(1)
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li/div/div[2]/div[2]/div/div/div/button[1]')))
            button = self.driver.find_element_by_xpath('//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li/div/div[2]/div[2]/div/div/div/button[1]')
            action.move_to_element(button).perform()
            button.click()
            time.sleep(1)
        except TimeoutException as e:
            logging.error(f'Timeout waiting for Terms of Use Policy to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling Terms of use Policy: {e}')
            return
        # Do you have any of the following life-threatening symptoms?
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div/div[3]/div[1]/main/div/section[2]/div/div/div/div/div/div/div/div/ul/li[3]/div/div[2]/div[2]/div/div/div/button[2]')))
            button = self.driver.find_element_by_xpath('/html/body/div[1]/div/div[3]/div[1]/main/div/section[2]/div/div/div/div/div/div/div/div/ul/li[3]/div/div[2]/div[2]/div/div/div/button[2]')
            # action.move_to_element(button).perform()
            time.sleep(1)
            button.click()
            time.sleep(1)
        except TimeoutException as e:
            logging.error(f'Timeout waiting for "Do you have any of the following life-threatening symptoms?" to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling "Do you have any of the following life-threatening symptoms?": {e}')
            return

        # Which state will you be in to receive the COVID-19 vaccine?
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div/div[3]/div[1]/main/div/section[2]/div/div/div/div/div/div/div/div/ul/li[5]/div/div[2]/div[2]/div/div/div/select')))
            select = Select(self.driver.find_element_by_xpath('/html/body/div[1]/div/div[3]/div[1]/main/div/section[2]/div/div/div/div/div/div/div/div/ul/li[5]/div/div[2]/div[2]/div/div/div/select'))
            select.select_by_value(self.state)
            time.sleep(1)
        except TimeoutException as e:
            logging.error(f'Timeout waiting for "Which state will you be in to receive the COVID-19 vaccine?" to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling "Which state will you be in to receive the COVID-19 vaccine?": {e}')
            return
        
        # What is your date of birth?
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[6]/div/div[2]/div[2]/div/div/div/div/form/div[1]/input')))
            text_box = self.driver.find_element_by_xpath('//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[6]/div/div[2]/div[2]/div/div/div/div/form/div[1]/input')
            text_box.send_keys(self.birthday)
            time.sleep(1)
        except TimeoutException as e:
            logging.error(f'Timeout waiting for "What is your date of birth?" to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling "What is your date of birth?": {e}')
            return

        # submit button
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[6]/div/div[2]/div[2]/div/div/div/div/form/div[2]/button')))
            button = self.driver.find_element_by_xpath('//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[6]/div/div[2]/div[2]/div/div/div/div/form/div[2]/button')
            button.click()
            time.sleep(1)
        except TimeoutException as e:
            logging.error(f'Timeout waiting for "Submit Button" to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling "Submit Button": {e}')
            return
        # have you received your first dose of covid vaccine?
        try:
            # no button:
            # //*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[7]/div/div[2]/div[2]/div/div/div/button[2]
            # yes button:
            # //*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[7]/div/div[2]/div[2]/div/div/div/button[1]
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[7]/div/div[2]/div[2]/div/div/div/button[2]')))
            button = self.driver.find_element_by_xpath('//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[7]/div/div[2]/div[2]/div/div/div/button[2]')
            # action.move_to_element(button).click().perform()
            button.click()
            time.sleep(1)
        except TimeoutException as e:
            logging.error(f'Timeout waiting for "Have you received your first dose of covid vaccine?" to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling "Have you received your first dose of covid vaccine?": {e}')
            return

        # Have you received a vaccine in the past 14 days?
        try:
            # no button:
            # //*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[9]/div/div[2]/div[2]/div/div/div/button[2]
            # yes button:
            # //*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[9]/div/div[2]/div[2]/div/div/div/button[1]
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[9]/div/div[2]/div[2]/div/div/div/button[2]')))
            button = self.driver.find_element_by_xpath('//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[9]/div/div[2]/div[2]/div/div/div/button[2]')
            # action.move_to_element(button).perform()
            time.sleep(1)
            button.click()
            time.sleep(1)
        except TimeoutException as e:
            logging.error(f'Timeout waiting for "Have you received your first dose of covid vaccine?" to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling "Have you received your first dose of covid vaccine?": {e}')
            return

        # schedule button
        try:
            # You are eligible for the COVID-19 vaccine! Please click the button below to schedule your vaccination appointment.
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[11]/div/div[2]/div[2]/div/div/div/button')))
            button = self.driver.find_element_by_xpath('//*[@id="content"]/div/section[2]/div/div/div/div/div/div/div/div/ul/li[11]/div/div[2]/div[2]/div/div/div/button')
            # action.move_to_element(button).perform()
            button.click()
            time.sleep(1)
        except TimeoutException as e:
            logging.error(f'Timeout waiting for "Schedule Button" to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling "Schedule Button": {e}')
            return

        # Location (city/state/zip)
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'distance')))
            select = Select(self.driver.find_element_by_name('distance'))
            select.select_by_value('20')  # max distance
        except TimeoutException as e:
            logging.error(f'Timeout waiting for "distance dropdown" to show: {e}')
            return
        except Exception as e:
            logging.error(f'Some error when handling "distance dropdown": {e}')
            return
        while True:
            for zipcode in self.zipcodes:
                try:
                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'location')))
                    location = self.driver.find_element_by_name('location')
                    location.clear()
                    location.send_keys(zipcode)
                except TimeoutException as e:
                    logging.error(f'Timeout waiting for "location text box" to show: {e}, but continuing...')
                    continue
                except Exception as e:
                    logging.error(f'Some error when handling "location text box": {e}, but continuing...')
                    continue
                button = self.driver.find_element_by_class_name('LocationDatePicker-button')
                button.click()
                # cards list
                try:
                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'MultiDoseScheduler')))
                    scheduler_list = self.driver.find_element_by_class_name('MultiDoseScheduler')
                except TimeoutException as e:
                    logging.error(f'Timeout waiting for "MultiDoseScheduler" to show: {e}, but continuing...')
                    continue
                except Exception as e:
                    logging.error(f'Some error when handling "MultiDoseScheduler": {e}, but continuing...')
                    continue
                # appointment cards
                try:
                    WebDriverWait(self.driver, 2).until(EC.presence_of_element_located((By.CLASS_NAME, 'AppointmentsByLocationCard')))
                    scheduler_cards = scheduler_list.find_elements_by_class_name('AppointmentsByLocationCard')
                    if len(scheduler_cards) == 0:
                        logging.info("no appointment cards found")
                    else:
                        for scheduler in scheduler_cards:
                            if not scheduler:
                                continue
                            card = Appointment(kroger_data=scheduler.text)
                            if card.vaccine_available:
                                # send notification
                                self.push_notification(str(card))
                                logging.info(str(card))
                            else:
                                logging.info(f"no appointment available at {card.store} ({card.street_address}, {card.city_state_zip})")
                except TimeoutException:
                    logging.info(f"no appointments found for zipcode {zipcode}, moving to next zipcode")
                    # unable to find available card after 2s, so let's re-search for appointments
                    continue


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--headless', dest='headless', default=False,
                        action='store_true', help='whether you want to run the browser in headless mode')
    parser.add_argument('--zipcodes', dest='zipcodes', nargs='+',
                        help='zipcodes to search for, you must provide one or more zipcodes for your state')
    parser.add_argument('--state', dest='state', type=str,
                        help='state to search vaccine for, you must provide one state')
    parser.add_argument('--birthday', dest='birthday', type=str,
                        help='your date of birth in the form: "mm/dd/YYYY" (e.g. 03/19/1968)')
    parser.add_argument('--pushbulletApiKey', dest='pb_api_key', type=str,
                        help='your pushhbulletAppointment API key used for notifications')
    parser.add_argument('--pushbulletDevice', dest='pb_device', type=str,
                        help='your pushhbullet device nickname for notifications (note: note pushed to all devices if no specific device provided)')
    args = parser.parse_args()
    
    availability_checker = AvailabilityChecker(
            enable_headless=args.headless,
            state=args.state,
            zipcodes=args.zipcodes,
            birthday=args.birthday,
            pb_api_key=args.pb_api_key,
            pb_device_name=args.pb_device,
        )
    availability_checker.check_kroger()
