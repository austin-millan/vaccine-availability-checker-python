import unittest
from py_pkg.pkg import AvailabilityChecker, Appointment


class TestAvailabilityChecker(unittest.TestCase):

    def test_check_kroger(self):
        availability_checker = AvailabilityChecker(
            enable_headless=False,
            state="OR",
            zipcodes=["97026"],
            birthday="03/19/1968",
            # pb_api_key="o.yourkey",
            # pb_device_name="your phone"
        )
        availability_checker.check_kroger()


class TestAppointment(unittest.TestCase):
    
    def test_parse_kroger_appointment_card(self):
        # test on good data
        appointment_available_str = 'South Salem\n15.63 mi\nAvailable Appointments for May 4\n3450 Commercial StSe\nSalem, OR 97302\nPharmacy Phone: (503) 585-3533\nPharmacy Hours\nSun: 11:00 AM - 6:00 PM\nMon - Fri: 9:00 AM - 9:00 PM\nSat: 9:00 AM - 7:00 PM\n1st Dose\nSaturday\nApr 24\n-----\n-----\n-----\n-----\nSunday\nApr 25\n-----\n-----\n-----\n-----\nMonday\nApr 26\n-----\n-----\n-----\n-----\nShow More Times\nContinue to 2nd Dose'
        card = Appointment(kroger_data=appointment_available_str)
        self.failIf(card.general_area != 'South Salem')
        self.failIf(card.vaccine_available)
        self.failIf(card.street_address != '3450 Commercial StSe')
        appointment_available_str = 'East Salem\n11.89 mi\nNo Available Appointments for this location.\n3740 Market StNe\nSalem, OR 97301\nPharmacy Phone: (503) 370-4351\nPharmacy Hours\nSun: 11:00 AM - 6:00 PM\nMon - Fri: 9:00 AM - 9:00 PM\nSat: 9:00 AM - 7:00 PM\nCheck Another Location\nThere are no more appointments available at this location. Please select another location.\nContinue to 2nd Dose'
        card = Appointment(kroger_data=appointment_available_str)
        self.failIf(card.general_area != 'East Salem')
        self.failIf(card.vaccine_available)
        self.failIf(card.street_address != '3740 Market StNe')


if __name__ == '__main__':
    unittest.main()
